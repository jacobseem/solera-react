import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, withRouter } from 'react-router';
import classnames from 'classnames/bind';
import { Dash, Secure } from 'containers';
import 'assets/css/index.css';
import styl from './app.styl';

const cx = classnames.bind(styl);

// DISABLE @builtin TypeScript and JavaScript Language Features

class App extends Component<{}> {
  render () {
    return (
      <div className={cx({ appBox: true })}>
        <div className={cx({ appBoxInner: true })}>
          <div className={cx({ appBoxLayers: true })}>
            <div className={cx({ appBoxLayer: true })}>
              <Switch>
                <Route exact path={'/'} render={(props) => <Dash {...props}/>} />
                <Route path={'/secure'} render={(props) => <Secure {...props}/>} />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state
});

export default withRouter(connect(mapStateToProps, null)(App));
