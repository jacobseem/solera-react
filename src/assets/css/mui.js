import { createMuiTheme } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors/orange';

const MuiTheme = createMuiTheme({
  palette: {
    primary: orange
  }
});

export default MuiTheme;
