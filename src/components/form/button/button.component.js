import React, { Component } from 'react';
import classnames from 'classnames/bind';
import { Button as MuiButton } from '@material-ui/core';
import styl from './button.component.styl';

const cx = classnames.bind(styl);

class Button extends Component<{
  children: any
}> {
  render () {
    const { children, ...custom } = this.props;
    return (
      <MuiButton {...custom} className={cx({ Button: true })} fullWidth>{children}</MuiButton>
    );
  }
}

export default Button;
