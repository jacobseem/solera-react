import React, { Component } from 'react';
import TestRenderer from 'react-test-renderer;
import Button from './button.component';

describe('Render' () => {
  it('renders correctly', () =>
    const wrapper = TestRenderer.create(<Button/>).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
