// @flow
import React, { Component } from 'react';
import classnames from 'classnames/bind';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { FormControl, InputLabel, Input as MuiInput } from '@material-ui/core';
import MuiTheme from 'assets/css/mui';
import styl from './control.component.styl';

const cx = classnames.bind(styl);

class Control extends Component<{
  input: any,
  label: string,
  meta: any
}> {
  render () {
    const { input, label, meta: { touched, error }, ...custom } = this.props;
    return (
      <MuiThemeProvider theme={MuiTheme}>
        <FormControl className={cx({ Control: true })} margin={'normal'} required={input.required} fullWidth>
          <InputLabel htmlFor={input.name}>{label}</InputLabel>
          <MuiInput className={cx({ MuiInput: true })} {...input} {...custom} error={touched && error} />
        </FormControl>
      </MuiThemeProvider>
    );
  }
}

export default Control;
