import React, { Component } from 'react';
import TestRenderer from 'react-test-renderer;
import control from './control.component';

describe('Render' () => {
  it('renders correctly', () =>
    const wrapper = TestRenderer.create(<control/>).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
