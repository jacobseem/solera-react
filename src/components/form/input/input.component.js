// @flow
import React, { Component } from 'react';
import classnames from 'classnames/bind';
import { withTheme } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';
import styl from './input.component.styl';

const cx = classnames.bind(styl);


class Input extends Component<{
  input: any,
  label: string,
  meta: any
}> {
  render () {
    const { input, label, meta: { touched, error }, ...custom } = this.props;
    return (
      <TextField className={cx({ TextField: true })} {...input} {...custom} placeholder={label} error={touched && error} fullWidth/>
    );
  }
}

export default withTheme()(Input);
