import React, { Component } from 'react';
import TestRenderer from 'react-test-renderer;
import Input from './input.component';

describe('Render' () => {
  it('renders correctly', () =>
    const wrapper = TestRenderer.create(<Input/>).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
