// @flow
import React, { Component } from 'react';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import { FormControlLabel, Switch as MuiSwitch } from '@material-ui/core';
import MuiTheme from 'assets/css/mui';
import classnames from 'classnames/bind';
import styl from './switch.component.styl';

const cx = classnames.bind(styl);

const styles = theme => ({
  iOSSwitchBase: {
    '&$iOSChecked': {
      color: theme.palette.common.white,
      '& + $iOSBar': {
        backgroundColor: '#F27E20'
      }
    },
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
      easing: theme.transitions.easing.sharp
    })
  },
  iOSChecked: {
    transform: 'translateX(15px)',
    '& + $iOSBar': {
      opacity: 1,
      border: 'none'
    }
  },
  iOSBar: {
    borderRadius: 13,
    width: 42,
    height: 26,
    marginTop: -13,
    marginLeft: -21,
    border: 'solid 1px',
    borderColor: theme.palette.grey[400],
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border'])
  },
  iOSIcon: {
    width: 24,
    height: 24
  },
  iOSIconChecked: {
    boxShadow: theme.shadows[1]
  }
});

class Switch extends Component<{
  classes: any,
  input: any,
  label: string,
  iOS: bool
}> {
  render () {
    const { classes, input, label, iOS } = this.props;
    return (
      <MuiThemeProvider theme={MuiTheme}>
        <FormControlLabel
          control={
            <MuiSwitch
              checked={input.value}
              value={input.name}
              onClick={input.onChange}
              color={'primary'}
              classes={iOS && {
                switchBase: classes.iOSSwitchBase,
                bar: classes.iOSBar,
                icon: classes.iOSIcon,
                iconChecked: classes.iOSIconChecked,
                checked: classes.iOSChecked,
              }}
            />
          }
          label={label}
        />
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(Switch);
