import React, { Component } from 'react';
import TestRenderer from 'react-test-renderer;
import Switch from './switch.component';

describe('Render' () => {
  it('renders correctly', () =>
    const wrapper = TestRenderer.create(<Switch/>).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
