import React, { Component } from 'react';
import TestRenderer from 'react-test-renderer';
import Dash from './dash.container';

describe('Render', () => {
  it('renders correctly', () => {
    const wrapper = TestRenderer.create(<Dash/>).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
