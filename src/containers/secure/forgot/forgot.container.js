// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { Typography } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import { Control, Switch, Button } from 'components/form';
import classnames from 'classnames/bind';
import styl from './forgot.container.styl';

const cx = classnames.bind(styl);

class Forgot extends Component<{
  handleSubmit: any,
  pristine: bool,
  submitting: bool
}> {

  handleSubmit (value) {
    console.log(value);
  }

  render () {
    const { handleSubmit, pristine, submitting } = this.props;
    return (
      <form className={cx('animated fadeIn', { Form: true })} onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
        <Link to={'/secure'}><ArrowBack className={cx({ ArrowBack: true })}/></Link>
        <Typography variant={'display1'} className={cx({ FormTitle: true, Minimal: true })}>Esqueceu sua senha? <br/> Não se preocupe.</Typography>

        <Field type={'text'} name={'email'} label={'E-mail'} component={Control} />

        <Button type={'submit'} variant={'contained'} disabled={pristine || submitting}>Redefinir Senha</Button>
      </form>
    );
  }
}

export default reduxForm({
  form: 'Forgot'
})(Forgot);
