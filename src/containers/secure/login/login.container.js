// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { Typography } from '@material-ui/core';
import { Control, Switch, Button } from 'components/form';
import classnames from 'classnames/bind';
import styl from './login.container.styl';

const cx = classnames.bind(styl);

class Login extends Component<{
  handleSubmit: any,
  pristine: bool,
  submitting: bool
}> {

  handleSubmit (value) {
    console.log(value);
  }

  render () {
    const { handleSubmit, pristine, submitting } = this.props;
    return (
      <form className={cx('animated fadeIn', { Form: true })} onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
        <Typography variant={'display2'} className={cx({ FormTitle: true })}>Bem vindo ao <br/> i360!</Typography>

        <Field type={'text'} name={'identifier'} label={'Usuário'} component={Control} />
        <Field type={'password'} name={'password'} label={'Senha'} component={Control} />
        <Field name={'remember'} label={'Continuar conectado'} component={Switch} />

        <Button type={'submit'} variant={'contained'} disabled={pristine || submitting}>Entrar</Button>
        <Link to={'/secure/forgot'} className={cx({ ForgotText: true })}>Esqueci minha senha</Link>
      </form>
    );
  }
}

export default reduxForm({
  form: 'Login'
})(Login);
