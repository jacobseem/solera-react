import React, { Component } from 'react';
import TestRenderer from 'react-test-renderer';
import Login from './login.container';

describe('Render', () => {
  it('renders correctly', () => {
    const wrapper = TestRenderer.create(<Login/>).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
