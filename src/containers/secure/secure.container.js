// @flow
import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import classnames from 'classnames/bind';
import styl from './secure.container.styl';

import Login from './login/login.container';
import Forgot from './forgot/forgot.container';

const cx = classnames.bind(styl);

class Secure extends Component<{
  match: Object
}> {
  render () {
    const { match } = this.props;
    return (
      <div className={cx('animated fadeIn', { SecureContainer: true })}>
        <div className={cx({ SecureForm: true })}>
          <Route exact path={`${match.url}`} component={Login}/>
          <Route exact path={`${match.url}/forgot`} component={Forgot}/>
        </div>

        <div className={cx({ SecureWall: true })}>
          <div className={cx({ SecureSoleraLogo: true })}></div>
        </div>
      </div>
    );
  }
}

export default Secure;
