import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import lang from './lang';

const reducers = combineReducers({
  routing,
  form: formReducer,
  lang
});

export default reducers;

