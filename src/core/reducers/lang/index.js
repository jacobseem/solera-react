import actionTypes from 'core/config/types';

export default function (state = { current: 'pt-br' }, action) {
  switch (action.type) {
    case actionTypes.SWITCH_LANGUAGE:
      return { ...state, current: action.payload };
    default:
      return state;
  }
}
