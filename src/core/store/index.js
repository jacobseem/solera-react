import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import createBrowserHistory from 'core/history';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import reducers from 'core/reducers';
import sagas from 'core/sagas';

const middlewares = [];

middlewares.push(routerMiddleware(createBrowserHistory));
const sagaMiddleware = createSagaMiddleware();
middlewares.push(sagaMiddleware);

const middleware = applyMiddleware(...middlewares);
const store = createStore(reducers, middleware);
const history = syncHistoryWithStore(createBrowserHistory, store);

sagaMiddleware.run(sagas);

export { store, history };
